## License plate detection

Detection based on Haar-cascades.

### Dependecies installation

Python 3.x is required, however Python 3.7 is not supported (because of Tensorflow). All Python package dependencies are listed in `requirements.txt` file. To install the with `pip`, execute following command in current directory:

```bash
pip install -r requirements.txt
```

### Main system

To use licence plate detection system, execute following command in current directory:
```bash
python alpr --input input_path --output output_path
```
where *input_path* is input image filepath, and *output_path* is a filepath where you'd like to save the output. Note that if there is no detection, there will be no output.  
The `--output` parameter is optional. If it's not specified, program output will be printed on screen (if there is any).  
Instead of `--input` parameter, you can specify `--dir` parameter, which accepts a path to directory with input images. The program then proceeds to process them one by one.

### Synthetic data generator

To use our synthetic licence plate data generator, execute following command in current directory:
```bash
python data_synthesizer backgrounds_dir plates_dir output_dir num_images
```
where:
- *backgrounds_dir* is a directory containing background images
- *plates_dir* is a directory containing high quality, non-skewed licence plate images (could be synthetic)
- *output_dir* is a directory, where gerated data will be outputted
- *num_images* specifies how many images should be generated

### Platesmania scraper

This scraper was created using the Scrapy framework. In order to use it, you 
should either install Scrapy locally or use the Docker image.

```bash
docker pull vimagick/scrapyd
cd data_scraper
docker run -it -v ${PWD}:/project vimagick/scrapyd bash
``` 

Then inside the container:
```bash
$ cd /project
$ scrapy crawl platesmania -o file.json
```

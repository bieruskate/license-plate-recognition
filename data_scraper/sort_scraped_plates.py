import argparse
import json
import os
import shutil


def sort_plates(scraped_info_file_path, scraped_images_dirpath):
    with open(scraped_info_file_path, 'r') as f:
        scraped_info = json.load(f)

    full_img_dir = os.path.join(scraped_images_dirpath, 'full_imgs')
    plates_dir = os.path.join(scraped_images_dirpath, 'plates')

    os.mkdir(full_img_dir)
    os.mkdir(plates_dir)

    for info in scraped_info:
        shutil.move(
            os.path.join(scraped_images_dirpath, info['full_img_file']),
            full_img_dir
        )
        shutil.move(
            os.path.join(scraped_images_dirpath, info['plate_img_file']),
            plates_dir
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Sort scrapped plate data.'
    )
    parser.add_argument('scraped_info_filepath', type=str)
    parser.add_argument('scraped_images_dirpath', type=str)
    args = parser.parse_args()

    scraped_info_filepath = args.scraped_info_filepath
    scraped_images_dirpath = args.scraped_images_dirpath

    sort_plates(scraped_info_filepath, scraped_images_dirpath)

import argparse
import json


def remove_duplicates(scraped_info_file_path):
    with open(scraped_info_file_path, 'r') as f:
        info = json.load(f)

    deduped_info = [dict(t) for t in set(tuple(d.items()) for d in info)]

    with open(scraped_info_file_path, 'w') as f:
        json.dump(deduped_info, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Remove duplicates from scraped info in json format.'
    )
    parser.add_argument('scraped_info_filepath', type=str)
    scraped_info_filepath = parser.parse_args().scraped_info_filepath

    remove_duplicates(scraped_info_filepath)

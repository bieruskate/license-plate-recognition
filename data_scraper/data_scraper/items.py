import scrapy


class LicensePlateItem(scrapy.Item):
    text = scrapy.Field()
    image_urls = scrapy.Field()
    full_img_file = scrapy.Field()
    plate_img_file = scrapy.Field()

import os
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.exceptions import DropItem


class LicensePlateImagePipeline(ImagesPipeline):
    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if not image_paths:
            raise DropItem('Item contains no images')

        del item['image_urls']
        item['full_img_file'], item['plate_img_file'] = \
            [os.path.split(path)[1] for path in image_paths]

        return item

import scrapy
from ..items import LicensePlateItem


class PlatesmaniaSpider(scrapy.Spider):
    name = 'platesmania'
    start_urls = [
        'http://platesmania.com/pl/gallery'
    ]
    custom_settings = {
        'ITEM_PIPELINES': {
            'data_scraper.pipelines.LicensePlateImagePipeline': 1
        },
        'IMAGES_STORE': './plate_images'
    }

    def __init__(self, num_pages=1):
        self.num_pages = int(num_pages)
        self.current_page = 1

    def parse(self, response):
        plates_info = response.css('.panel-body')

        for info in plates_info:
            plate_images = info.css('img')
            img_urls = plate_images.css('::attr(src)').extract()
            img_urls[0] = img_urls[0].replace("/m/", "/o/")

            licence_plate_item = LicensePlateItem(
                text=plate_images[1].attrib['alt'],
                image_urls=img_urls
            )
            yield licence_plate_item

        next_page_url = response.css('.pagination')[0]\
            .css('a::attr(href)')[-1].extract()
        if self.current_page < self.num_pages and \
           next_page_url and next_page_url != '#':
            self.current_page += 1
            yield response.follow(next_page_url)


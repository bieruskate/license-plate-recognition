import argparse
import json
import os


def convert_info(info_path, img_path, output_path):
    with open(info_path, 'r') as f:
        info = json.load(f)

    rel_img_path = os.path.relpath(
        img_path,
        os.path.dirname(output_path)
    )

    with open(output_path, 'w') as f:
        for data in info:
            path = os.path.join(rel_img_path, data['filename'])
            bb = ' '.join(map(str, data['outer_bb']))
            f.write(f'{path} 1 {bb}\n')
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert generated info from json to opencv cascade format.'
    )
    parser.add_argument('generated_info_filepath', type=str)
    parser.add_argument('generated_images_dirpath', type=str)
    parser.add_argument('output_filepath', type=str)
    args = parser.parse_args()

    convert_info(
        args.generated_info_filepath,
        args.generated_images_dirpath,
        args.output_filepath
    )

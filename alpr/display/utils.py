import cv2 as cv


def draw_bbox_with_detected_text(img, text,
                                 x, y,
                                 w, h,
                                 bbox_color=(0, 90, 255),
                                 bbox_thickness=2,
                                 text_color=(0, 0, 210),
                                 text_thickness=2):
    cv.rectangle(img,
                 (x, y), (x + w, y + h),
                 color=bbox_color,
                 thickness=bbox_thickness)
    cv.putText(img, text, (x, y),
               cv.FONT_HERSHEY_DUPLEX,
               max(w / 250, .5), text_color,
               text_thickness)
    return img

import argparse
from pathlib import Path

from alpr import alpr


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Licence plate detector')
    parser.add_argument('--input')
    parser.add_argument('--dir', default=None)
    parser.add_argument('--output', default=None)

    args = parser.parse_args()
    input_path = args.input
    input_dir = args.dir
    output_path = args.output

    if input_dir:
        for input_path in Path(input_dir).glob("*.jpg"):
            print(input_path)
            alpr(str(input_path), output_path)
    else:
        alpr(input_path, output_path)
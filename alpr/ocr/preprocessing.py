from PIL import Image
import numpy as np
from skimage import exposure


def get_background_color(char):
    if len(char[char > 100]) == 0:
        return 255
    return int(np.median(char[char > 100]))


def preprocess_image(img, desired_size):
    img = (exposure.equalize_hist(img) * 255).astype(np.uint8)

    im = Image.fromarray(img)
    old_size = im.size

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    im = im.resize(new_size, Image.ANTIALIAS)

    bg_color = get_background_color(img)
    padded_im = Image.new("L", (desired_size, desired_size), color=bg_color)

    padded_im.paste(im, ((desired_size - new_size[0]) // 2,
                         (desired_size - new_size[1]) // 2))

    return padded_im

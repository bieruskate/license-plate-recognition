import string

import numpy as np
from keras_preprocessing import image

from ocr.preprocessing import preprocess_image

IMG_SHAPE = 28
LABELS = string.digits + '?' + string.ascii_uppercase.replace('Q', '')


def predict_character(img, model):
    x = preprocess_image(img, IMG_SHAPE)
    x = image.img_to_array(x)
    x = np.expand_dims(x, axis=0)
    x = x / 255
    predictions = model.predict(x)

    return LABELS[np.argmax(predictions)]

import cv2 as cv
import numpy as np

from display.utils import draw_bbox_with_detected_text
from processing.char_detection import CharDetector
from processing.plate_detection import PlateDetector
from processing.tree import create_bounding_box_tree, get_chars_for_node
from processing.utils import parse_plate_string

from constants import CASCADES


BASE_MIN_NEIGHBOURS = 1
MIN_BLOBS = 5


def alpr(input_image_path, output_image_path=None):
    res = 'empty'
    for cas_idx, cascade in enumerate(CASCADES):
        plate_detector = PlateDetector(str(cascade), BASE_MIN_NEIGHBOURS)
        char_detector = CharDetector()

        image = cv.imread(input_image_path)

        coords = plate_detector.detect(image)
        if len(coords) == 0:
            continue

        tree = create_bounding_box_tree(coords)

        unchecked_nodes_set = set(tree)
        leaf_nodes = [n for n in tree if n.is_leaf]

        def get_chars_len(chars):
            return len([c for c in chars if c != '?'])

        best_nodes = []
        for node in leaf_nodes:
            best_chars = []
            best_chars_bbs = []
            best_node = None
            while True:
                if node not in unchecked_nodes_set:
                    break
                unchecked_nodes_set.remove(node)

                chars, chars_bbs = get_chars_for_node(node, char_detector,
                                                      image)
                if len(chars) < 15 and get_chars_len(chars) > get_chars_len(
                        best_chars):
                    best_chars = chars
                    best_chars_bbs = chars_bbs
                    best_node = node
                else:
                    break

                if node.is_root:
                    break
                node = node.parent
            if get_chars_len(best_chars) >= MIN_BLOBS:
                best_node.chars = best_chars
                best_node.chars_bbs = best_chars_bbs
                best_nodes.append(best_node)

        best_nodes_set = set(best_nodes)
        for node in best_nodes:
            best_nodes_set -= set(node.ancestors)

        res = [(n.bb, n.chars_bbs, parse_plate_string("".join(n.chars))) for n
               in best_nodes_set]
        if len(res):
            break

    updated_res = []

    for bb, chars_bbs, text in res:
        bbs = np.array(chars_bbs)
        min_row, min_col = np.amin(bbs[:, [0, 1]], axis=0)
        max_row, max_col = np.amax(bbs[:, [2, 3]], axis=0)

        display_offset = 5

        fixed_to_chars_bb = np.array([
            chars_bbs[0][1] + bb[0],
            bb[1] + min_row,
            max_col - min_col,
            max_row - min_row
        ])

        bb_with_display_offset = fixed_to_chars_bb
        updated_res.append((bb, chars_bbs, text, fixed_to_chars_bb))

        bb_with_display_offset[:2] -= display_offset
        bb_with_display_offset[2:] += display_offset * 2

        draw_bbox_with_detected_text(image, text, *bb_with_display_offset,
                                     text_thickness=1,
                                     text_color=(255, 255, 255))
        for char_bb in chars_bbs:
            min_row, min_col, max_row, max_col = \
                np.array(char_bb) + np.array([bb[1], bb[0]] * 2)
            cv.rectangle(image,
                         (min_col, min_row), (max_col, max_row),
                         color=(255, 97, 0),
                         thickness=1)
    if len(res):
        if output_image_path:
            cv.imwrite(output_image_path, image)
        else:
            cv.imshow('Auto', image)
            cv.waitKey(0)

    return updated_res, cas_idx

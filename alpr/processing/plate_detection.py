import cv2 as cv


class PlateDetector:
    def __init__(self, cascade_path, base_min_neighbours=5):
        self.cascade = cv.CascadeClassifier(cascade_path)
        self.base_min_neighbours = base_min_neighbours

    def detect(self, image):
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

        detections = self._try_detect(gray)
        return detections

    def _try_detect(self, gray):
        nb_neighbours = self.base_min_neighbours
        detections = self._get_detection(gray, nb_neighbours)

        while len(detections) == 0 and nb_neighbours >= 0:
            nb_neighbours -= 1
            detections = self._get_detection(gray, nb_neighbours)

        return detections

    def _get_detection(self, gray, nb_neighbours):
        return self.cascade.detectMultiScale(gray, minNeighbors=nb_neighbours)

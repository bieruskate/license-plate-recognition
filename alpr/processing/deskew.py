import cv2
import numpy as np
from skimage.feature import corner_harris, corner_peaks
from skimage.filters import rank
from skimage.measure import label, regionprops
from skimage.morphology import disk
from skimage.util import pad
from sklearn.cluster import KMeans

PAD_WIDTH = 5  # could be different, doesn't affect anything much


def deskew_potential_plate(img, thr=0.1, test_parallelogram=False):
    bin_img = img > rank.otsu(img, disk(10))

    cc = label(bin_img)
    regs = regionprops(cc)

    if not regs:
        return

    max_region = max(regs, key=lambda r: r.area)
    max_region_convex = max_region.convex_image

    padded_convex = pad(
        max_region_convex,
        pad_width=PAD_WIDTH,
        mode='constant',
        constant_values=0
    )
    corners = corner_peaks(corner_harris(padded_convex)) - PAD_WIDTH
    corners = np.flip(corners, axis=1)

    if len(corners) < 4:
        return

    if len(corners) > 4:
        model = KMeans(4)
        model.fit(corners)
        corners = model.cluster_centers_.astype(int)

    bounding_box = max_region.bbox

    sorted_corners = _sort_corners(corners)

    if test_parallelogram:
        is_parallelogram = _test_if_parallelogram(thr, bounding_box,
                                                  sorted_corners)
        if not is_parallelogram:
            return

    min_row, min_col, _, _ = bounding_box
    sorted_corners += np.array([min_col, min_row])

    img_rows, img_cols = img.shape
    img_max_x = img_cols - 1
    img_max_y = img_rows - 1
    new_corners = np.float32([
        [0, 0], [img_max_x, 0], [img_max_x, img_max_y], [0, img_max_y]
    ])

    M = cv2.getPerspectiveTransform(np.float32(sorted_corners), new_corners)
    dst = cv2.warpPerspective(np.float32(img), M, (img_cols, img_rows))

    return np.uint8(dst)


def _sort_corners(corners):
    left_right_sort = corners[corners[:, 0].argsort()]
    left = left_right_sort[:2]
    right = left_right_sort[2:]

    tl, bl = left[left[:, 1].argsort()]
    tr, br = right[right[:, 1].argsort()]

    return [tl, tr, br, bl]


def _test_if_parallelogram(threshold, bounding_box, sorted_corners):
    min_row, min_col, max_row, max_col = bounding_box
    bb_width = max_col - min_col
    bb_height = max_row - min_row

    tl, tr, br, bl = sorted_corners

    atol_x, atol_y = threshold * bb_width, threshold * bb_height
    c1 = (tl + br) / 2
    c2 = (tr + bl) / 2
    return np.isclose(c1[0], c2[0], atol=atol_x) and \
           np.isclose(c1[1], c2[1], atol=atol_y)

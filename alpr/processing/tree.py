from anytree import AnyNode

from .utils import cut_detection, calculate_bounding_box_area, \
    calculate_rectangles_intersection_area


def create_bounding_box_tree(bboxes):
    bboxes = reversed(sorted(bboxes, key=calculate_bounding_box_area))
    tree_nodes = []

    for candidate_bb in bboxes:
        candidate_parent = None
        for node in tree_nodes:
            x1, y1, w1, h1 = node.bb
            x2, y2, w2, h2 = candidate_bb

            inter_area = calculate_rectangles_intersection_area(
                x1, x1 + w1, y1, y1 + h1,
                x2, x2 + w2, y2, y2 + h2
            )

            if inter_area / calculate_bounding_box_area(candidate_bb) >= 0.6:
                candidate_parent = node
        if candidate_parent is None:
            tree_nodes.append(AnyNode(bb=candidate_bb))
        else:
            tree_nodes.append(AnyNode(bb=candidate_bb, parent=candidate_parent))

    return tree_nodes


def get_chars_for_node(node, char_detector, image):
    region = cut_detection(image, node.bb)
    chars = char_detector.get_characters(region, convert_to_gray=True)
    return chars

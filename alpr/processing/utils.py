import cv2 as cv

from constants import PLATE_REGIONS


def convert_to_greyscale(image):
    return cv.cvtColor(image, cv.COLOR_BGR2GRAY)


def cut_detection(image, detection, greyscale=False):
    """Cut detection from image: detection format: (x, y, w, h)"""
    (x, y, w, h) = detection
    cut = image[y:y + h, x:x + w]

    if greyscale:
        return convert_to_greyscale(cut)
    return cut


def get_scaled_window_slice(x, y, w, h, img_shape, scale_factor):
    assert scale_factor >= 1

    x1 = int(max(round(x - (scale_factor - 1) * w), 0))
    y1 = int(max(round(y - (scale_factor - 1) * h), 0))
    w = int(round(w * scale_factor))
    h = int(round(h * scale_factor))

    x2 = min(x + w, img_shape[1] - 1)
    y2 = min(y + h, img_shape[0] - 1)

    return slice(y1, y2), slice(x1, x2)


def calculate_bounding_box_area(bb):
    _, _, w, h = bb
    return w * h


def calculate_rectangles_intersection_area(min_x1, max_x1, min_y1, max_y1,
                                           min_x2, max_x2, min_y2, max_y2):
    x_intersect = max(0, min(max_x1, max_x2) - max(min_x1, min_x2))
    y_intersect = max(0, min(max_y1, max_y2) - max(min_y1, min_y2))
    return x_intersect * y_intersect


def parse_plate_string(plate_string):
    plate_string = plate_string.strip('?')
    normal_plate = not plate_string.replace('?', '').isalpha()
    if not normal_plate:
        return plate_string

    region_stop_pos = 3 if plate_string[:3] in PLATE_REGIONS else 2

    region_code = plate_string[:region_stop_pos].replace('0', 'O')
    car_id = plate_string[region_stop_pos:].replace('O', '0')
    return region_code + car_id

import numpy as np
from keras.models import load_model
from scipy import ndimage
from skimage import filters, measure, morphology

import ocr.prediction as ocr
from processing.utils import convert_to_greyscale

from constants import OCR_PATH


class CharDetector:
    MODEL = load_model(OCR_PATH)
    MIN_AREA = 15

    def get_characters(self, img, convert_to_gray=True):
        if convert_to_gray:
            img = convert_to_greyscale(img)

        bin_img = img < filters.rank.otsu(img, morphology.square(
            min(img.shape) // 2))

        labeled_img = measure.label(bin_img)
        props = measure.regionprops(labeled_img)

        props = [p for p in props if p.area > self.MIN_AREA]
        props = sorted(props, key=lambda p: p.centroid[1])

        props = self._apply_filter(props, self._drop_by_weight_to_height_ratio)
        props = self._apply_filter(props,
                                   self._drop_by_location,
                                   img.shape[1] / 5, 1)
        props = self._apply_filter(props, self._drop_outliers_by_height)

        bboxes = [p.bbox for p in props]
        chars = [self._cut_img_region_with_bbox(img, bb) for bb in bboxes]
        char_preds = [ocr.predict_character(c, self.MODEL) for c in chars]

        return char_preds, bboxes

    @staticmethod
    def _apply_filter(props, filter_method, *args):
        if len(props) > 0:
            return filter_method(props, *args)
        return props

    def _drop_outliers_by_height(self, props):
        heights = np.array([p.bbox[2] - p.bbox[0] for p in props])
        outliers_mask = self._reject_outliers_mask_std(heights, m=1.9)
        return np.array(props)[outliers_mask]

    @staticmethod
    def _drop_by_weight_to_height_ratio(props):
        widths = np.array([p.bbox[3] - p.bbox[1] for p in props])
        heights = np.array([p.bbox[2] - p.bbox[0] for p in props])
        ratios = widths / heights
        ratio_mask = (0.25 < ratios) & (ratios < 0.9)

        solidities = np.array([p.solidity for p in props])
        solidity_mask = (solidities > 0.8) & (0.10 < ratios) & (ratios < 0.15)

        return np.array(props)[ratio_mask | solidity_mask]

    @staticmethod
    def _drop_by_location(props, max_distance, centroid_dim):
        if len(props) < 5:
            return []

        centroids = np.array([p.centroid for p in props])
        distances = np.array([c2[centroid_dim] - c1[centroid_dim]
                              for c1, c2 in zip(centroids[:-1], centroids[1:])])

        max_distance_mask = distances <= max_distance
        if not np.any(max_distance_mask):
            return []

        labeled_mask, num_labels = ndimage.label(max_distance_mask)
        largest_label = max(range(1, num_labels + 1),
                            key=lambda l: np.sum(labeled_mask == l))

        good_idxs = np.argwhere(labeled_mask == largest_label)
        good_idxs = np.append(good_idxs, good_idxs[-1] + 1)

        return np.array(props)[good_idxs]

    @staticmethod
    def _cut_img_region_with_bbox(base_img, bbox):
        min_row, min_col, max_row, max_col = bbox
        return base_img[min_row:max_row, min_col:max_col]

    @staticmethod
    def _reject_outliers_mask(data, m=2.):
        if len(data) == 0:
            return data

        data = np.array(data)
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = (d / mdev) if mdev else np.zeros(len(d))
        return s < m

    @staticmethod
    def _reject_outliers_mask_std(data, m=1.):
        if len(data) == 0:
            return data

        data = np.array(data)
        d = np.abs(data - np.mean(data))
        std = np.std(data)
        s = (d / std) if std else np.zeros(len(d))
        return s <= m

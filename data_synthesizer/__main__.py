import argparse
import synth_pipeline


parser = argparse.ArgumentParser(description='Licence plate synthetic data generator')
parser.add_argument('backgrounds_dir', type=str)
parser.add_argument('plates_dir', type=str)
parser.add_argument('output_dir', type=str)
parser.add_argument('num_images', type=int)

args = parser.parse_args()

synth_pipeline.synthesize_data(
    args.backgrounds_dir,
    args.plates_dir,
    args.output_dir,
    args.num_images
)

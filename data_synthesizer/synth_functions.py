import cv2
import numpy as np


def _transform_bounding_box(bb, aff_mtx):
    new_bb = np.append(bb, [[1,1,1,1]], axis=0)
    new_bb = np.matmul(aff_mtx, new_bb)
    return np.around(new_bb).astype(int)

def _get_bounding_box_rectangle_dimensions(bb):
    min_x = bb[0, :].min()
    max_x = bb[0, :].max()
    min_y = bb[1, :].min()
    max_y = bb[1, :].max()

    return max_x - min_x, max_y - min_y

def _get_move_to_origin_matrix(bb):
    move_left = bb[0, :].min()
    move_up = bb[1, :].min()
    return np.float32([[1, 0, -move_left], [0, 1, -move_up]])

def _compose_affine_matrices(a, b):
    b = np.append(b, [[0, 0, 1]], axis=0)
    return np.matmul(a, b)

def _perform_affine_transform_and_keep_origin(img, bb, aff_mtx):
    new_bb = _transform_bounding_box(bb, aff_mtx)
    move_mtx = _get_move_to_origin_matrix(new_bb)
    new_bb = _transform_bounding_box(new_bb, move_mtx)

    composed_mtx = _compose_affine_matrices(move_mtx, aff_mtx)
    new_dims = _get_bounding_box_rectangle_dimensions(new_bb)
    new_img = cv2.warpAffine(img, composed_mtx, new_dims)

    return new_img, new_bb 


def mutate_image(img, fake_alpha_channel=True):
    if fake_alpha_channel:
        img[img != 255] += 1

    rows, cols = img.shape[0], img.shape[1]
    # bounding box columns: tl, tr, br, bl
    bb = np.float32([[0, 0], [cols-1, 0], [cols-1, rows-1], [0, rows-1]]).T

    # Rotate
    rot_dg = np.random.uniform(-30, 30)
    rot_mtx = cv2.getRotationMatrix2D((cols/2,rows/2), rot_dg, 1)
    img, bb = _perform_affine_transform_and_keep_origin(img, bb, rot_mtx)

    # Skew
    pts1 = np.float32(bb.T[:3])
    pts2 = np.float32(pts1 + np.random.uniform(-10, 10, size=pts1.shape))
    aff_mtx = cv2.getAffineTransform(pts1, pts2)
    img, bb = _perform_affine_transform_and_keep_origin(img, bb, aff_mtx)

    return img, bb.T


def paste_image_fully_visible(dst, src, inner_bb, min_scale=0.5, max_scale=1.5,
                              bg_color=0):
    dst_y_dim, dst_x_dim = dst.shape[:2]
    src_y_dim, src_x_dim = src.shape[:2]

    max_scale = min(dst_x_dim / src_x_dim, dst_y_dim / src_y_dim, max_scale)
    resize_scale = min_scale
    if max_scale > min_scale:
        resize_scale = np.random.uniform(min_scale, max_scale)

    res_src = cv2.resize(src, None, fx=resize_scale, fy=resize_scale)
    res_scr_y_dim, res_scr_x_dim = res_src.shape[:2]

    paste_origin_x, paste_origin_y = np.floor([
        np.random.uniform(0, dst_x_dim - res_scr_x_dim),
        np.random.uniform(0, dst_y_dim - res_scr_y_dim)
    ]).astype(int)

    outer_bb = np.array([ # tl.x, tl.y, w, h
        paste_origin_x, paste_origin_y, res_scr_x_dim, res_scr_y_dim
    ])

    inner_bb = inner_bb.copy()
    inner_bb = np.around(inner_bb * resize_scale).astype(int)
    inner_bb += outer_bb[:2]

    paste_x_slice = slice(paste_origin_x, paste_origin_x + res_scr_x_dim)
    paste_y_slice = slice(paste_origin_y, paste_origin_y + res_scr_y_dim)

    img = dst.copy()
    img[paste_y_slice, paste_x_slice][res_src != bg_color] = \
        res_src[res_src != bg_color]

    return img, outer_bb, inner_bb

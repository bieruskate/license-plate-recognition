import os
import random
import json

import numpy as np
import cv2

import synth_functions


def _git_files_filter(filepath):
    filename = os.path.basename(filepath)
    return filename != '.gitignore' and filename != '.gitkeep'

def synthesize_data(dst_dir, src_dir, output_dir, num_data):
    dst_filenames = list(filter(_git_files_filter, os.listdir(dst_dir)))
    src_filenames = list(filter(_git_files_filter, os.listdir(src_dir)))

    bb_list = []

    for idx in range(num_data):
        dst_filename = os.path.join(dst_dir, random.choice(dst_filenames))
        src_filename = os.path.join(src_dir, random.choice(src_filenames))

        dst_img = cv2.imread(dst_filename)
        src_img = cv2.imread(src_filename)

        img, inner_bb = synth_functions.mutate_image(src_img)
        img, outer_bb, inner_bb = synth_functions.paste_image_fully_visible(
            dst_img, img, inner_bb
        )

        # random brightness
        lum_val = np.random.randint(0, 100, dtype=img.dtype)
        brighten_or_darken = np.random.randint(0, 2)
        if brighten_or_darken:
            img[img <= 255 - lum_val] += lum_val
        else:
            img[img >= lum_val] -=lum_val

        # random blur
        blur = random.randint(1, 10)
        blur = blur if blur % 2 else blur - 1
        img = cv2.GaussianBlur(img, (blur,) * 2, 0)

        img_filename = f'{idx}.png'

        bb_list.append({
            'filename': img_filename,
            'outer_bb': outer_bb.tolist(),
            'inner_bb': inner_bb.tolist()
        })
        cv2.imwrite(os.path.join(output_dir, img_filename), img)

    with open(os.path.join(output_dir, 'bb_info.json'), 'w') as f:
        json.dump(bb_list, f)
